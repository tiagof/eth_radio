// ----------------------------------------------------------------------------
// (c) 2020 KaRadio
// ----------------------------------------------------------------------------

#ifndef __have__TempHumSensor_h__
#define __have__TempHumSensor_h__

#include "interface.h"

bool initTempHum();
bool getTempHumid(bool check_if_new);
void temp_hum();

#endif // __have__TempHumSensor_h__
